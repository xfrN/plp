

function submit(url, data, success, error) {

	var asyn = true;

	if (typeof data == 'function') {
		error = success;
		success = data;
		data = null;
	}

	var request = new XMLHttpRequest();
	request.open('GET', url, asyn);

	if (asyn) {
		request.onload = function() {
			if (request.status == 200) {
				try {
					data = JSON.parse(request.responseText);
				} catch($e) {
					data = request.responseText;
				}
				if (typeof success == 'function') success(data);
			} else {
				if (typeof error == 'function') error(request.status);
			}
		};
		request.onerror = function(err) {
			if (typeof error == 'function') error(err);
		};
	}

	if (data) {
		request.send(data);
	} else {
		request.send();
	}

	if (!asyn) {
		if (request.status == 200) {
			try {
				data = JSON.parse(request.responseText);
			} catch($e) {
				data = request.responseText;
			}
			if (typeof success == 'function') success(data);
		} else {
			if (typeof error == 'function') error();
		}
	}
}




function load(lang, sportId) {
	submit('https://line02.bkfon-resource.ru/line/mobile/showEvents?lang=' + lang +'&lineType=full_line&sportId=' + sportId, function(data) {
		var index = -1;		

		function render() {
			index = (index + 1) % data.events.length;
			var event = data.events[index];

			document.querySelector("[data-content=EVENT]").innerText = event.sportName;
			document.querySelector("[data-content=STARTTIME]").innerText = event.startTime;
			//document.querySelector("[data-content=STARTDATE]").innerText = event.startTime;
			//document.querySelector("[data-content=LOGO1]").setAttribute("src",window.data["LOGO1"]);
			//document.querySelector("[data-content=LOGO2]").setAttribute("src",window.data["LOGO2"]);
			document.querySelector("[data-content=TEAM1]").innerText = event.team1;
			document.querySelector("[data-content=TEAM2]").innerText = event.team2;
			// for (var i = 0; i < event.subcategories.length; i++){
				var quotes = event.subcategories[0].quotes;
			
					document.querySelector("[data-content=QUOTE1]").innerText = quotes[0].quote;
					document.querySelector("[data-content=QUOTE2]").innerText = quotes[1].quote;
					document.querySelector("[data-content=QUOTEX]").innerText = quotes[2].quote;
				
			// }			
		}
		setInterval(render, 60000);
		render();
	});
}

