$(document).ready(function(){
  const userLang = navigator.language || navigator.userLanguage;
  console.log ("The language is: " + userLang); 
  switch(userLang) {
    case 'en': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-gb': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-us': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-ca': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-au': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-bz': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-au': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-ie': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-jm': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-nz': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-ph': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-za': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-tt': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'en-zw': 
    $.MultiLanguage('./assets/js/language.json', 'en');
    break;
    case 'ru':
    $.MultiLanguage('./assets/js/language.json', 'ru');
    break;
    case 'ru-mo':
    $.MultiLanguage('./assets/js/language.json', 'ru');
    break;
    case 'ru-RU':
    $.MultiLanguage('./assets/js/language.json', 'ru');
    break;
    case 'uk':
    $.MultiLanguage('./assets/js/language.json', 'uk');
    break;
    case 'uz':
    $.MultiLanguage('./assets/js/language.json', 'uz');
    break;
    case 'uz-UZ':
    $.MultiLanguage('./assets/js/language.json', 'uz');
    break;
    case 'kz':
    $.MultiLanguage('./assets/js/language.json', 'ru');
    break;
    case 'kk':
    $.MultiLanguage('./assets/js/language.json', 'kk');
    break;
    case 'kk-KZ':
    $.MultiLanguage('./assets/js/language.json', 'kk');
    break;
    case 'az':
    $.MultiLanguage('./assets/js/language.json', 'az');
    break;
    case 'az-AZ':
    $.MultiLanguage('./assets/js/language.json', 'az');
    break;
    case 'hy':
    $.MultiLanguage('./assets/js/language.json', 'hy');
    break;
    case 'hy-AM':
    $.MultiLanguage('./assets/js/language.json', 'hy');
    break;
    case 'el':
    $.MultiLanguage('./assets/js/language.json', 'el');
    break;
    case 'el-GR':
    $.MultiLanguage('./assets/js/language.json', 'el');
    break;
    default:
    $.MultiLanguage('./assets/js/language.json', 'en');
}

});