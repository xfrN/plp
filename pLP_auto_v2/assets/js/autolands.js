if ((window.location.pathname.substr(-1) != '/') && (window.location.pathname.substr(-5) != '.html')) window.location.pathname += '/';

function get_alias(aliases, pathname) {

  // var alias = false;
  // for (var host in aliases) {
  //   if ('fonbet.ru'.search(host) > -1) {
  //     for (var path in aliases[host]) {
  //       if (pathname.search(path) > -1) {
  //         alias = aliases[host][path]
  //         break;
  //       }
  //     }
  //     break;
  //   }
  // }

  return {
      
      "type": "filter",
      "sort": "rand",
      "filter": {
        "topic": "лига чемпионов уефа|лига европы уефа|испания\\. премьер-лига|испания\\. кубок"
      }
    };
}

function get_outcomes(event) {
  return {
    1: event.outcome_1 ? (event.outcome_1.attributes ? event.outcome_1.attributes.factor_value : event.outcome_1['@attributes'].factor_value) : '',
    2: event.outcome_2 ? (event.outcome_2.attributes ? event.outcome_2.attributes.factor_value : event.outcome_2['@attributes'].factor_value) : '',
    3: event.outcome_3 ? (event.outcome_3.attributes ? event.outcome_3.attributes.factor_value : event.outcome_3['@attributes'].factor_value) : '',
  };
}

function get_event(events, filter) {

  var event = false;

  if (alias.type == 'id') for (var e in events) if (events[e].event_id == alias.id) event = events[e];

  if (alias.type == 'regex') {
    var id = window.location.pathname.match(alias.regex)[1];
    for (var e in events) if (events[e].event_id == id) event = events[e];
  }

  if (alias.type == 'filter') {

    var list = [];
    var filter = false;

    for (var e in events) {

      events[e].outcomes = get_outcomes(events[e]);

      filter = true;
      if (!events[e].outcomes[1] || (!events[e].outcomes[2] && !events[e].outcomes[3])) filter = false;
      if (filter) {
        for (var f in alias.filter) {
          if (events[e][f]) {
            if (Array.isArray(alias.filter[f])) {
              if (alias.filter[f].indexOf(events[e][f]) == -1) filter = false;
            } else {
              try {
                if (events[e][f].search(new RegExp(alias.filter[f], 'iu')) < 0) filter = false;
              } catch (error) {
                if (events[e][f] != alias.filter[f]) filter = false;
              }
            }
          }
        }
      }

      if (filter) list.push(events[e]);
    }

    if (alias.sort == 'last') event = list.pop();
    if (alias.sort == 'first') event = list.shift();
    if (alias.sort == 'rand') event = list[Math.floor(Math.random() * list.length)];
  }

  return event;
}

function render_event(event, alias) {

  var days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
  var months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

  event.date = new Date(event.start_date2 + '+03:00');

  if (!event.outcomes) event.outcomes = get_outcomes(event);

  if (!alias.bg || alias.bg === false) render_bg('https://logo.fonstats.ru/images/bg/' + event.sport_type_id + '.jpg');

  event.logo1 = 'https://logo.fonstats.ru/' + (alias.img || 'logo') + 's/' + event.sport_type_id + '/' + event.full1 + '.png?left';
  event.logo2 = 'https://logo.fonstats.ru/' + (alias.img || 'logo') + 's/' + event.sport_type_id + '/' + event.full2 + '.png?right';

  if (jQuery('[data-content=URL]').length) {
    var search = jQuery('[data-content=URL]')[0].search;
    jQuery('[data-content=URL]').attr('href', event.url);
    jQuery('[data-content=URL]')[0].search = search;
  }
  if (jQuery('[data-content=URL_MOBILE]').length) {
    var search = jQuery('[data-content=URL_MOBILE]')[0].search;
    jQuery('[data-content=URL_MOBILE]').attr('href', event.url_mobile);
    jQuery('[data-content=URL_MOBILE]')[0].search = search;
  }

  jQuery('[data-content=TOPIC]').text(event.topic);
  jQuery('[data-content=EVENT]').val(event.event_title).text(event.event_title);
  jQuery('[data-content=STARTDATE]').text(event.date.getDate() + ' ' + months[event.date.getMonth()]);
  jQuery('[data-content=STARTTIME]').text((event.date.getHours() < 10 ? '0' : '') + event.date.getHours() + ':' + (event.date.getMinutes() < 10 ? '0' : '') + event.date.getMinutes());
  jQuery('[data-content=STARTDOTE]').text((event.date.getDate() < 10 ? '0' : '') + event.date.getDate() + '.' + ((event.date.getMonth() + 1) < 10 ? '0' : '') + (event.date.getMonth() + 1));
  jQuery('[data-content=LOGO1]').attr('src', event.logo1).addClass(alias.img);
  jQuery('[data-content=LOGO2]').attr('src', event.logo2).addClass(alias.img);
  jQuery('[data-content=TEAM1]').text(event.team1);
  jQuery('[data-content=TEAM2]').text(event.team2);
  jQuery('[data-content=BET1]').val(event.team1);
  jQuery('[data-content=BETX]').val('vs');
  jQuery('[data-content=BET2]').val(event.team2);
  jQuery('[data-content=QUOTE1]').text(event.outcomes[1]);
  if (event.outcomes[2] && event.outcomes[3]) jQuery('[data-content=QUOTEX]').text(event.outcomes[2]);
  jQuery('[data-content=QUOTE2]').text(event.outcomes[3] || event.outcomes[2]);

  if (event.team1 == 'да' && event.team2 == 'нет') {
    jQuery('[data-content=NOBOOL]').hide();
    jQuery('[data-content=BOOL]').show();
  }
}

function render_alias(alias) {
  if (alias.brand === false) jQuery('[data-content=BRAND] > *').hide();
  if (alias.landing) jQuery('[data-content=LANDING]').val(alias.landing);
  if (alias.logo) jQuery('[data-content=LOGO], [data-content=LOGO_MOBILE]').attr('src', alias.logo);
  if (alias.logo_mobile) jQuery('[data-content=LOGO_MOBILE]').attr('src', alias.logo_mobile);
  if (alias.bg) render_bg(alias.bg);
  if (alias.bg_mobile && window.innerWidth < 768) render_bg(alias.bg_mobile);
}

function render_bg(bg) {
  jQuery('[data-content=BG], [data-content=BG_ERROR]').css('background-image', 'url(' + bg + ')');
  jQuery('[data-content=BG_LEFT]').addClass('self-left');
}

function render_sorry(alias) {
  if (alias.title) jQuery('[data-content=TITLE]').text(alias.title);
  jQuery('[data-content=SORRY]').show();
}

function render_error() {
  jQuery('[data-content=OK]').hide();
  jQuery('[data-content=ERROR]').show();
}

function run() {
  jQuery.get('https://text.fonstats.ru/texts/autolands.json', function(aliases) {

    alias = get_alias(aliases, window.location.pathname);

    if (alias) {

      // console.log(alias);

      render_alias(alias);

      if (!alias.feed) alias.feed = 'autolands';

      jQuery.get('https://feed.fonstats.ru/feeds/autolands.json', function(events) {

        var event = get_event(events.event, alias.filter);

        if (!event && alias.empty) {

          render_sorry(alias);

          alias = get_alias(aliases, alias.empty);

          if (alias) {

            // console.log(alias);

            render_alias(alias);

            event = get_event(events.event, alias.filter);
          }
        }

        if (event) {
          console.log(event);

          render_event(event, alias);
        } else {
          render_error();
        }
      }, 'json');
    } else {
      render_error();
    }
  }, 'json');
}

jQuery(document).ready(function () {
  run();
});